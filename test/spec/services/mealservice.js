'use strict';

describe('Service: MealService', function () {
  // utils
  var MealsData = [{
        'id': 'cad2d2e8b16eb668f47b4f2827438951',
        'name': 'Seafood risotto',
        'description': 'This seafood risot',
        'price': '9.50',
        'tags': ['main_courses', 'pescetarian', 'seafood']
    },{
        'id': '6308e806b1233e111081666e82f7aac1',
        'name': 'Penne bacon, green peas',
        'description': 'Caramelized onions',
        'price': '8.50',
        'tags': ['pasta']
    }];

  // load the service's module
  beforeEach(module('jstestApp'));

  // instantiate service
  var MealService;
  beforeEach(inject(function ($injector) {
	MealService = $injector.get('MealService');
  }));


  it('should do something', function () {
    expect(!!MealService).toBe(true);
  });

  it('should create a new meal with main and qty props', function () {
    var m = new MealService(MealsData[0]).meal;
    expect(m.main).toBe(true);
    expect(m.qty).toBe(1);
  });
});
