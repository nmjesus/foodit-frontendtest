'use strict';

describe('Service: OrderService', function () {
  // utils
  var MealsData = [{
        'id': 'cad2d2e8b16eb668f47b4f2827438951',
        'name': 'Seafood risotto',
        'description': 'This seafood risot',
        'price': '9.50',
        'tags': ['main_courses', 'pescetarian', 'seafood']
    },{
        'id': '6308e806b1233e111081666e82f7aac1',
        'name': 'Penne bacon, green peas',
        'description': 'Caramelized onions',
        'price': '8.50',
        'tags': ['pasta']
    }];

  // load the service's module
  beforeEach(module('jstestApp'));

  // instantiate service
  var OrderService;
  beforeEach(inject(function ($injector) {
	OrderService = $injector.get('OrderService');
    OrderService.checkout();
  }));


  it('should do something', function () {
    expect(!!OrderService).toBe(true);
  });

  it('Should add a meal to main dishes', function () {
    OrderService.add(MealsData[0]);
    expect(OrderService.meals.main.length).not.toBe(0);
  });

  it('Should add a meal to other dishes', function () {
    OrderService.add(MealsData[1]);
    expect(OrderService.meals.other.length).not.toBe(0);
  });

  it('Should have total', function () {
    OrderService.add(MealsData[0]);
    expect(OrderService.getTotal()).not.toBe(0);
    expect(OrderService.getTotal()).toBe('9.50');
  });

  it('Should sum quantity when adding again the same meal', function () {
    OrderService.add(MealsData[0]);
    OrderService.add(MealsData[0]);
    expect(OrderService.getTotal()).toBe((9.5*2).toFixed(2));
  });

  it('Should update quantity', function () {
    OrderService.add(MealsData[0]);
    OrderService.updateQty(MealsData[0], 1);
    expect(OrderService.meals.main[0].qty).toBe(2);


    OrderService.updateQty(MealsData[0], -1);
    expect(OrderService.meals.main[0].qty).toBe(1);

    // don't go below 1
    OrderService.updateQty(MealsData[0], -1);
    expect(OrderService.meals.main[0].qty).toBe(1);
  });


  it('Should update total', function() {
    OrderService.add(MealsData[0]);
    OrderService.updateQty(MealsData[0], 1);
    expect(OrderService.getTotal()).toBe((9.5*2).toFixed(2));


    OrderService.updateQty(MealsData[0], -1);
    expect(OrderService.getTotal()).toBe((9.5).toFixed(2));

    // don't go below 1
    OrderService.updateQty(MealsData[0], -1);
    expect(OrderService.getTotal()).toBe((9.5).toFixed(2));
  });


  it('Should clear the order on checkout', function () {
    OrderService.add(MealsData[0]);
    OrderService.updateQty(MealsData[0], 1);
    expect(OrderService.getTotal()).toBe((9.5*2).toFixed(2));

    OrderService.checkout();
    expect(OrderService.getTotal()).toBe((0).toFixed(2));
  });


  it('Should load the previous order', function() {
    OrderService.add(MealsData[0]);
    OrderService.updateQty(MealsData[0], 1);
    expect(OrderService.getTotal()).toBe((9.5*2).toFixed(2));
    OrderService.meals = OrderService.clear();
    expect(OrderService.getTotal()).toBe((0).toFixed(2));

    OrderService.load();
    expect(OrderService.getTotal()).toBe((9.5*2).toFixed(2));
  });
});
