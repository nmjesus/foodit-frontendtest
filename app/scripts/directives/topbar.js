'use strict';

angular.module('jstestApp').directive('topbar', function () {
    return {
        templateUrl: 'views/topbar.html',
        restrict: 'A',
        controller: 'MainCtrl'
    };
});
