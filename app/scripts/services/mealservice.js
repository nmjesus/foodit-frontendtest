'use strict';

angular.module('jstestApp').factory('MealService', function () {
    var _isMain = function(tags) {
        var i, len = tags.length, _cur;
        for(i = 0; i<len; i++) {
            _cur = tags[i];
            if(_cur === 'main_courses') {
                return true;
            }
        }
        return false;
    };

    function MealService(meal) {
        this.meal = meal;
        this.meal.qty = 1;
        this.meal.main = _isMain(meal.tags);
    }

    return MealService;
});
