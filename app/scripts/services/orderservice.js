'use strict';

angular.module('jstestApp').factory('OrderService', ['MealService', function (MealService) {
    /* utils */ 
    var getMealById = function(id, meals) {
        var i, len = meals.length, _cur;
        for(i = 0; i<len; i++) {
            _cur = meals[i];
            if(_cur.id === id) {
                return _cur;
            }
        }
        return false;
    };

    function OrderService() {
        this.meals = this.clear();
        this.total = 0;

    }

    OrderService.prototype = {
        add: function(meal) {
            var mealAlreadyOnOrdered = getMealById(meal.id, this.meals.main.concat(this.meals.other));

            if(mealAlreadyOnOrdered) {
                return this.updateQty(mealAlreadyOnOrdered, 1);
            }

            var m = new MealService(meal).meal;

            var kind = m.main ? 'main' : 'other';
            this.meals[kind].push(m);

            return this.save(this.meals);
        },

        updateQty: function(meal, howMany) {
            if(meal.qty === 1 && howMany === -1) {
                return;
            }
            meal.qty += howMany;
            this.save();
            return meal.qty;
        },

        clear: function() {
            return {
                main: [],
                other: [],
            };
        },

        checkout: function() {
            this.meals = this.clear();
            return this.save();
        },

        save: function() {
            // avoid anonymous mode exception
            try {
                return localStorage.setItem('order:meals', JSON.stringify(this.meals));
            } catch(e) {
                return false;
            }
        },

        getTotal: function() {
            var meals = this.meals.main.concat(this.meals.other), i = 0, len = meals.length, _cur, total = 0;
            for(i = 0; i<len; i++) {
                _cur = meals[i];
                total += parseFloat(_cur.price) * _cur.qty;
            }
            this.total = total;
            return this.total.toFixed(2);
        },

        load: function() {
            // avoid anonymous mode exception
            try {
                return (this.meals = JSON.parse(localStorage.getItem('order:meals')) || this.meals);
            } catch(e) {
                return (this.meals = this.clear());
            }
        }
    };

    return new OrderService();
}]);
