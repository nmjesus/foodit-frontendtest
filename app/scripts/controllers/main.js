'use strict';

/**
 * @ngdoc function
 * @name jstestApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the jstestApp
 */
angular.module('jstestApp').controller('MainCtrl', ['$scope', 'MenuService', 'OrderService', function ($scope, MenuService, OrderService) {
        $scope.menu = {};
        $scope.order = OrderService;

        var fixTags = function(tags) {
            if(!tags) {
                return [];
            }
            for(var i = 0, len = tags.length; i<len; i++) {
                tags[i] = tags[i].replace(/#.*:/g, '');
            }
            return tags;
        };

        MenuService.get('/data/menu.json').success(function(data) {
            var _cur, meals = data && data.meals ? data.meals : [];
            for(var i = 0, len = meals.length; i<len; i++) {
                _cur = meals[i];
                _cur.tags = fixTags(_cur.tags);
            }
            $scope.menu = data;
        });
    }
]);
